import React from "react";
import ReactDOM from "react-dom";

const App: React.FC = () => {
  const css = `
  .popup {
    background-color: green;
  }
  `

  return (<div className='popup'>
    <style>
      {css}
    </style>
    <h1>01_example_popup</h1>
  </div>);
};


document.addEventListener("DOMContentLoaded", () => {
  const rootEl = document.getElementById("root");
  return ReactDOM.render(<App />, rootEl);
});
