const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtensionReloader = require('webpack-extension-reloader');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const { EnvironmentPlugin } = require('webpack');
const remotedev = require('remotedev-server');
const TerserPlugin = require("terser-webpack-plugin");

//const child_process = require('child_process');

module.exports = (env, argv) => {
  return {
    entry: {
      popup: `${__dirname}/src/popup/index.tsx`,
      options: `${__dirname}/src/options/index.tsx`,
      background: `${__dirname}/src/background/index.ts`,
      content: `${__dirname}/src/content/index.tsx`,
    },
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: '[name].js',
    },
    devtool: false,
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: 'ts-loader',
          exclude: /node_modules/,
        },
        { test: /\.css$/, use: ['style-loader', 'css-loader'] },
        // {
        //   test: /\.(?:ico|gif|png|jpg|jpeg)$/i,
        //   type: 'asset/resource',
        // },
        {
          test: /\.(gif|png|jpe?g)$/i,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name]-[hash].[ext]',
                outputPath: 'img/',
                publicPath: 'img/',
              },
            },
            {
              loader: 'image-webpack-loader',
              options: {
                disable: true, // webpack@2.x and newer
              },
            },
          ],
        },
        {
          test: /\.svg$/,
          use: ['@svgr/webpack', 'file-loader'],
        },
      ],
    },
    
    resolve: {
      extensions: ['.tsx', '.ts', '.js'],
      plugins: [
        new TsconfigPathsPlugin({
          /* options: see below */
        }),
      ],
    },
    plugins: [
      new EnvironmentPlugin(['mode', 'USE_REDUX_DEV_TOOLS', 'LANGUAGE']),
      new CopyWebpackPlugin({
        patterns: [
          {
            from: './manifest.json',
            force: true,
          },
          {
            from: './src/popup/popup.html',
            force: true,
          },
          {
            from: './src/options/options.html',
            force: true,
          },
          {
            from: './src/background/background.html',
            force: true,
          },
          {
            from: './src/img/icon16.png',
            to: './img',
            force: true,
          },
          {
            from: './src/img/icon24.png',
            to: './img',
            force: true,
          },
          {
            from: './src/img/icon32.png',
            to: './img',
            force: true,
          },
          {
            from: './src/img/icon48.png',
            to: './img',
            force: true,
          },
          {
            from: './src/img/icon56.png',
            to: './img',
            force: true,
          },
          {
            from: './src/img/icon128.png',
            to: './img',
            force: true,
          },
          {
            from: './src/img/icon256.png',
            to: './img',
            force: true,
          }
        ],
      }),
      new ExtensionReloader({
        port: 9090, // Which port use to create the server
        reloadPage: true, // Force the reload of the page also
        entries: {
          // The entries used for the content/background scripts
          // Use the entry names, not the file name or the path
          background: 'background', // *REQUIRED
          extensionPage: 'popup',
          contentScript: 'content'
        },
      }),
    ],
    devServer: argv.watch
      ? remotedev({ hostname: 'localhost', port: 8000 })
      : undefined,
    optimization: {
      minimizer: [
        new TerserPlugin({
          terserOptions: {
            format: {
              comments: false,
            },
          },
          extractComments: false,
        }),
      ],
    },
  };
};
