import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Injection } from '../dist/index';

describe('it', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(
      <Injection
        position="beforeend"
        selectTargetElement={() => document.querySelector('body')}
        containerClassName="extension-portal-injection"
      >
        <p>test</p>
      </Injection>,
      div
    );
    ReactDOM.unmountComponentAtNode(div);
  });
});
