<a name="0.2.33"></a>
## [0.2.33](http://bitbucket.org/cupcakedev/cupcake-injection-npm/compare/v0.2.32...v0.2.33) (2021-05-21)


### Bug Fixes

* refresh screenshots ([09a3b2b](http://bitbucket.org/cupcakedev/cupcake-injection-npm/commits/09a3b2b))



<a name="0.2.32"></a>
## [0.2.32](http://bitbucket.org/cupcakedev/cupcake-injection-npm/compare/v0.2.31...v0.2.32) (2021-05-21)


### Bug Fixes

* run pipelines webhook from master branch only ([c780d2c](http://bitbucket.org/cupcakedev/cupcake-injection-npm/commits/c780d2c))



<a name="0.2.31"></a>
## [0.2.31](http://bitbucket.org/cupcakedev/cupcake-injection-npm/compare/v0.2.30...v0.2.31) (2021-05-21)



<a name="0.2.30"></a>
## [0.2.30](http://bitbucket.org/cupcakedev/cupcake-injection-npm/compare/v0.2.29...v0.2.30) (2021-05-21)


### Bug Fixes

* gitignore fix ([85b3d67](http://bitbucket.org/cupcakedev/cupcake-injection-npm/commits/85b3d67))



<a name="0.2.29"></a>
## [0.2.29](http://bitbucket.org/cupcakedev/cupcake-injection-npm/compare/v0.2.28...v0.2.29) (2021-05-21)


### Bug Fixes

* npm publish fix ([b36543c](http://bitbucket.org/cupcakedev/cupcake-injection-npm/commits/b36543c))



<a name="0.2.28"></a>
## [0.2.28](http://bitbucket.org/cupcakedev/cupcake-injection-npm/compare/v0.2.27...v0.2.28) (2021-05-21)


### Bug Fixes

* pipelines fix ([5cb5f8a](http://bitbucket.org/cupcakedev/cupcake-injection-npm/commits/5cb5f8a))



<a name="0.2.27"></a>
## [0.2.27](http://bitbucket.org/cupcakedev/cupcake-injection-npm/compare/v0.2.26...v0.2.27) (2021-05-21)


### Bug Fixes

* pipelines fix ([b30b8fa](http://bitbucket.org/cupcakedev/cupcake-injection-npm/commits/b30b8fa))
* pipelines fix ([7432afb](http://bitbucket.org/cupcakedev/cupcake-injection-npm/commits/7432afb))
* pipelines fix ([ac66e3d](http://bitbucket.org/cupcakedev/cupcake-injection-npm/commits/ac66e3d))



<a name="0.2.26"></a>
## [0.2.26](http://bitbucket.org/cupcakedev/cupcake-injection-npm/compare/v0.2.25...v0.2.26) (2021-05-21)



<a name="0.2.25"></a>
## [0.2.25](http://bitbucket.org/cupcakedev/cupcake-injection-npm/compare/v0.2.22-0...v0.2.25) (2021-05-21)


### Bug Fixes

* pipelines fix ([243a361](http://bitbucket.org/cupcakedev/cupcake-injection-npm/commits/243a361))
* pipelines fix ([6d42e37](http://bitbucket.org/cupcakedev/cupcake-injection-npm/commits/6d42e37))
* pipelines fix ([e47144f](http://bitbucket.org/cupcakedev/cupcake-injection-npm/commits/e47144f))
* publish script fix ([51b982a](http://bitbucket.org/cupcakedev/cupcake-injection-npm/commits/51b982a))
* publish script fix ([ba2ad22](http://bitbucket.org/cupcakedev/cupcake-injection-npm/commits/ba2ad22))



<a name="0.2.22-0"></a>
## [0.2.22-0](http://bitbucket.org/cupcakedev/cupcake-injection-npm/compare/v0.2.22...v0.2.22-0) (2021-05-21)


### Bug Fixes

* publish script fix ([3769fcf](http://bitbucket.org/cupcakedev/cupcake-injection-npm/commits/3769fcf))



<a name="0.2.22"></a>
## [0.2.22](http://bitbucket.org/cupcakedev/cupcake-injection-npm/compare/v0.2.21...v0.2.22) (2021-05-21)


### Bug Fixes

* pre-push fix ([bceeef9](http://bitbucket.org/cupcakedev/cupcake-injection-npm/commits/bceeef9))
* pre-push fix ([95f89eb](http://bitbucket.org/cupcakedev/cupcake-injection-npm/commits/95f89eb))
* pre-push fix ([59b0c30](http://bitbucket.org/cupcakedev/cupcake-injection-npm/commits/59b0c30))



<a name="0.2.21"></a>
## [0.2.21](http://bitbucket.org/cupcakedev/cupcake-injection-npm/compare/v0.2.20...v0.2.21) (2021-05-21)


### Bug Fixes

* pre-push fix ([3347279](http://bitbucket.org/cupcakedev/cupcake-injection-npm/commits/3347279))



<a name="0.2.20"></a>
## [0.2.20](http://bitbucket.org/cupcakedev/cupcake-injection-npm/compare/v0.2.19...v0.2.20) (2021-05-21)


### Bug Fixes

* pre-push fix ([f990f12](http://bitbucket.org/cupcakedev/cupcake-injection-npm/commits/f990f12))



<a name="0.2.19"></a>
## [0.2.19](http://bitbucket.org/cupcakedev/cupcake-injection-npm/compare/v0.2.18...v0.2.19) (2021-05-21)


### Bug Fixes

* pipelines fix ([3c8fe73](http://bitbucket.org/cupcakedev/cupcake-injection-npm/commits/3c8fe73))
* pipelines fix ([60a8d2e](http://bitbucket.org/cupcakedev/cupcake-injection-npm/commits/60a8d2e))
* pre-push fix ([3e81289](http://bitbucket.org/cupcakedev/cupcake-injection-npm/commits/3e81289))
* pre-push fix ([432780c](http://bitbucket.org/cupcakedev/cupcake-injection-npm/commits/432780c))



<a name="0.2.18"></a>
## [0.2.18](http://bitbucket.org/cupcakedev/cupcake-injection-npm/compare/v0.2.17...v0.2.18) (2021-05-20)


### Bug Fixes

* pre-push fix ([5258f5f](http://bitbucket.org/cupcakedev/cupcake-injection-npm/commits/5258f5f))



<a name="0.2.17"></a>
## 0.2.17 (2021-05-20)



