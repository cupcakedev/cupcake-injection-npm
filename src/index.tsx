import { ClassNameInjection } from './ClassNameInjection';
import { Injection } from './Injection';
const injectionModule = { ClassNameInjection, Injection };

export { ClassNameInjection, Injection };
export default injectionModule;
