# EXTENSION-INJECTION
NPM for injection react components via browser extension to current page  
##

## Installation
```npm i extension-injection```  
```yarn add extension-injection```
##

## Examples
![Code example](https://bitbucket.org/cupcakedev/cupcake-injection-npm/raw/master/screenshots/code_example.png "Code example")​  
![Page example 1](https://bitbucket.org/cupcakedev/cupcake-injection-npm/raw/master/screenshots/page_example_1.png "Page example 1")​  
![page example 1](https://bitbucket.org/cupcakedev/cupcake-injection-npm/raw/master/screenshots/page_example_2.png "page example 2")​ 
##

## License
MIT